# kanaloa

The name of a Hawaiin volcano, the _yin_ to Kāne's _yan_. A cephalopod (... octopus).

# Purpose

> Kicks of gitlab CI pipelines in response to other ones.

- To connect gitlab CI pipelines accross repositories. (Gitlab enterprise feature)
- (To test some ideas about clojurescript front-end work.)

# How to DEV

Open a terminal tab for this long-running process:

```
$ cd ui
$ npm run watch
```

This will use the npm toolchain (which is a dev-time dependency) to continuously watch & compile the cljs and css source into the target folder. The `target` folder is symlinked into the `service` top-level folder, when the clojure java process can then servie the javascript as JVM classpath resources.

Now you can open up the clj repl project in the `service/` folder in a new terminal tab (or you favourite IDE). The `service/dev` folder contains the dev-time clojure definitions to manage the lifecycle of the service in the repl.

(This project uses `consul` for a datastore, so you need to be in an environment where `CONSUL_HTTP_ADDR` is set and reachable.)

```
$ cd service
$ lein repl

User namespace being loaded
Compiling kanaloa.core
User namespace being loaded
nREPL server started on port 56187 on host 127.0.0.1 - nrepl://127.0.0.1:56187
REPL-y 0.4.3, nREPL 0.5.3
Clojure 1.10.0
Java HotSpot(TM) 64-Bit Server VM 1.8.0_111-b14
    Docs: (doc function-name-here)
          (find-doc "part-of-name-here")
  Source: (source function-name-here)
 Javadoc: (javadoc java-object-or-class-here)
    Exit: Control+D or (exit) or (quit)
 Results: Stored in vars *1, *2, *3, an exception in *e

kanaloa.core=> (in-ns 'user)
user=> (go)
...
```

Open your browser at [`http://localhost:8000`](http://localhost:8000).

# How to BUILD (and run...)

```
$ ( cd ui; npm run build ) && ( cd service; lein uberjar )
$ java -jar service/target/uberjar/kanaloa.jar
```

# Background

This project assumes the existence of so-called *environment-launchers*. Those special-purpose CI scripts know how to collect different artifacts and track version numbers for them. They also let _kanaloa_ know which other CI pipelines they depend on, with a crafted REST call. _kanaloa_ will then listen to gitlab for when those pipelines occur and kick off the launchers in response.

- A [*release project* example](https://gitlab.zoona.io/centaur/tachyon-launcher/).

# Technical

`kanaloa` is a clojure webservice:
- that accepts HTTP curl POSTS from gitlab CI pipelines
- that tracks build pipelines events
- by registering itself with build hooks in gitlab projects as appropriate
- notifies _release_ projects, as appropriate, when new artifacts they depend on is modified with a new build, by kicking of those CI pipelines.

- Uses the gitlab API a lot.
- clojurescript front-end.
- clojure back-end
- (websockets)
- (kafka)

