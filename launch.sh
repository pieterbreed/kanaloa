#!/bin/bash

set -e

echo "OK. we've arrived. We're deploying stuff now."

export VAULT_ADDR=${VAULT_ADDR:-http://vault.zoona-internal}
export CONSUL_HTTP_ADDR=${CONSUL_HTTP_ADDR:-http://consul-ui.zoona-internal}
export NOMAD_ADDR=${NOMAD_ADDR:-http://nomad.service.consul:4646}
export CI_COMMIT_REF_NAME=${CI_COMMIT_REF_NAME:-`git symbolic-ref -q --short HEAD || git describe --tags --exact-match`}

echo "CI_COMMIT_REF_NAME = $CI_COMMIT_REF_NAME"


if [[ ".$ROLE_ID" != "." && ".$SECRET_ID" != "." ]]; then
  tmpf=`mktemp`.json
  vault write -format=json \
  	auth/approle/login \
  	role_id="${ROLE_ID:?}" \
  	secret_id="${SECRET_ID:?}" > "$tmpf"
  export VAULT_TOKEN=`jq -r '.auth.client_token' < "$tmpf"`
fi

export FORCE_DEPLOY=`date +%s`

# just create it, for in case
mkdir -p renders

consul-template \
    -config config/common.hcl \
    -config config/log-shipper.hcl \
    -once \
    -log-level info

consul-template \
    -config config/common.hcl \
    -config config/kanaloa.hcl \
    -once \
    -log-level info
