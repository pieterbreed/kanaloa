template {
  source = "templates/kanaloa.hcl.tpl"
  destination = "renders/kanaloa.hcl"
  left_delimiter = "<("
  right_delimiter = ")>"
  command = "bash -c 'nomad run renders/kanaloa.hcl'"
}
