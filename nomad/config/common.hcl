wait {
  min = "5s"
  max = "10s"
}

consul {
  address = "http://consul-ui.zoona-internal"
  retry {
    enabled = false
  }
}

vault {
  address = "http://vault.zoona-internal"
  renew_token = false
  retry {
    enabled = false
  }
}
