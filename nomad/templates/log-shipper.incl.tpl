<( $ci := secret "secret/nomad_ci" )>
<( $log_beat_shipper_version := key "config/log_shipper/version" )>
        task "log-shipper" {
            driver = "docker"
            kill_timeout = "10s"

            config {
                image = "docker-registry.zoona.io/ops-engineering/nomad-beat-logstash-shipper:<( $log_beat_shipper_version )>"
                auth {<( with $ci )>
                  username = "<( .Data.username )>"
                  password = "<( .Data.password )>"<( end )>
                }
            }
    
            env {
                # nomad will fill this one in
                LOGSTASH_HOST = "${attr.unique.network.ip-address}"
            }
    
            resources {
                cpu = <( key "config/log_shipper/resource_cpu" )>
                memory = <( key "config/log_shipper/resource_memory" )>
                network {
                    mbits = <( key "config/log_shipper/resource_network_mbits" )>
                    port "http" {}
                }
            }
        }
