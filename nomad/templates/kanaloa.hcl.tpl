<( $ci := secret "secret/nomad_ci" )>
job "kanaloa" {
    datacenters = ["dc1"]
    type = "service"

    update {
        stagger = "2m"
        max_parallel = 1
    }

    group "kanaloa" {
        count = "2"

        restart {
            attempts = 5
            delay = "60s"
            interval = "10m"
            mode = "fail"
        }

        task "kanaloa" {
            leader = true
            driver = "docker"
            config {
                force_pull = true
                image = "docker-registry.zoona.io/ops-engineering/kanaloa/runtime:<( env "CI_COMMIT_REF_NAME" | trimSpace )>"
                auth {<( with $ci )>
                  username = "<( .Data.username )>"
                  password = "<( .Data.password )>"<( end )>
                }
                port_map {
                    http = 8000
                }
            }

            template {
                data = <<EOH
                {{with secret "secret/systems/kanaloa/gitlab"}}
GITLAB_TOKEN={{.Data.token}}{{ end }}
EXTERNAL_APP_URL=https://kanaloa.{{ key "config/global/TLD" }}
LOG_LEVEL={{ keyOrDefault "kanaloa/log-level" "info" }}
EOH
                destination = "secrets/vars"
                env = true
                change_mode = "restart"
            }

            vault {
                policies = ["kanaloa"]
            }

            env {
                FORCE_NOMAD_DEPLOY = "<( env "FORCE_DEPLOY" )>"
		LOG_HOST           = "${attr.unique.hostname}"
		LOG_FACILITY       = "${NOMAD_TASK_NAME}/${NOMAD_ALLOC_INDEX}"
            }


            resources {
                cpu = <( keyOrDefault "kanaloa/resources-cpu" "1000" )>
                memory = <( keyOrDefault "kanaloa/resources-memory" "300" )>
                network {
                    mbits = <( keyOrDefault "kanaloa/resources-network-mbits" "1" )>
                    port "http" {}
                }
            }

            service {
                name = "kanaloa"
                tags = [
                  "traefik.tags=traefik",
                  "traefik.frontend.rule=Host:kanaloa.<( key "config/global/TLD" )>"
                ]
                port = "http"
                check {
                    type = "http"
                    port = "http"
                    path = "/healthcheck"
                    interval = "5s"
                    timeout  = "2s"
                 }
            }
        }
<( file "renders/log-shipper.incl" )>
    }
}
