(defproject zoona.ops/kanaloa "0.1.0-SNAPSHOT"
  :repositories [["releases" {:url           "https://nexus.zoona.io/content/repositories/releases"
                              :sign-releases false
                              :username      [:gpg :env/nexus_write_username]
                              :password      [:gpg :env/nexus_write_password]}]
                 ["snapshots" {:url      "https://nexus.zoona.io/content/repositories/snapshots"
                               :username [:gpg :env/nexus_write_username]
                               :password [:gpg :env/nexus_write_password]}]]
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.clojure/core.async "0.4.490"]
                 [integrant "0.7.0"]
                 [integrant/repl "0.3.1" :scope "test"]
                 [http-kit "2.3.0"]
                 [ring/ring-core "1.7.1"]
                 [ring/ring-defaults "0.3.2"]
                 [compojure "1.6.1"]
                 [ring/ring-json "0.4.0"]
                 [clj-http "3.9.1"]
                 [ring/ring-codec "1.1.1"]
                 [beckon "0.1.1"]
                 [environ "1.1.0"]
                 [bk/ring-gzip "0.3.0"]
                 [ring/ring-anti-forgery "1.3.0"]
                 [org.clojure/tools.reader "1.3.2"]
                 [net.cgrand/xforms "0.19.0"]
                 [com.taoensso/encore "2.108.1"]
                 [com.taoensso/sente "1.14.0-RC2"]
                 [com.taoensso/timbre "4.10.0"]
                 [zoona.lib/gelf-timbre-appender "0.4.2"]]
  :plugins [[lein-ancient "0.6.15"]]
  :resource-paths #{"frontend" "src"}
  :aot [kanaloa.core]
  :main kanaloa.core
  :uberjar-name "kanaloa.jar"
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}
             :dev {:source-paths ["dev"]
                   :dependencies []}})
