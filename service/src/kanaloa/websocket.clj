(ns kanaloa.websocket
  (:require [clojure.set :as set]
            [clojure.core.async :as csp]
            [integrant.core :as ig]
            [taoensso.sente :as sente]
            [taoensso.sente.server-adapters.http-kit :as sente-http-kit]
            [taoensso.timbre :as timbre
             :refer [log  trace  debug  info  warn  error  fatal  report spy get-env]]))


(defmethod ig/init-key
  :adapter/websocket
  [_ _]
  (let [new-client-ch (csp/chan)
        state (sente/make-channel-socket!
               (sente-http-kit/get-sch-adapter)
               {:csrf-token-fn nil
                ;; this makes every user-id unique for every session
                ;; we don't have a concept of user identity
                :user-id-fn (fn [ring-req] (:client-id ring-req))})]
    (add-watch (:connected-uids state)
               :new-clients
               (fn [_ _ {old-any :any} {new-any :any}]
                 (let [d (set/difference new-any old-any)]
                   (when-not (empty? d)
                     (csp/go (csp/>! new-client-ch d))))))
    (assoc state
           :new-client-mult (csp/mult new-client-ch)
           :send-to-all-clients (fn [v]
                                  (csp/go
                                    (debug (str "About to send '" v "' to ALL clients: "))
                                    (doseq [uid (-> state :connected-uids deref :any)]
                                      (debug (str "-->" uid))
                                      (csp/go ((:send-fn state) uid v)))))
           :send-to-client (fn [uids v]
                             (debug (str "About to send " v " to client-id " uids))
                             (doseq [uid uids]
                               (csp/go ((:send-fn state) uid v))))
           :cleanup-fn #(do (remove-watch (:connected-uids state) :new-clients)
                            (csp/close! new-client-ch)))))

(defmethod ig/halt-key!
  :adapter/websocket
  [_ {cleanup-fn :cleanup-fn}]
  (cleanup-fn))
