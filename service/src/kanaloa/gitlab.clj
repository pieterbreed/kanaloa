(ns kanaloa.gitlab
  (:require [kanaloa.common.regex :as kan-regex]
            [kanaloa.self-declare :as decl]
            [kanaloa.consul-backup :as consul-backup]
            [kanaloa.vault :as vault]
            [kanaloa.consul-backup :as consul-backup]
            [clj-http.client :as http]
            [clojure.core.async :as csp]
            [clojure.spec.alpha :as s]
            [ring.util.codec :refer [url-encode]]
            [integrant.core :as ig]
            [cheshire.core :as json]
            [net.cgrand.xforms :as x]
            [taoensso.timbre :as timbre
             :refer [log  trace  debug  info  warn  error  fatal  report spy get-env]]))

(s/def ::project-name (s/and string? #(re-matches kan-regex/group-project-regex %)))
(s/def ::projects (s/coll-of ::project-name))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;

(defmethod ig/init-key :adapter/gitlab
  [_ opts]
  opts)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;


(defn get-project-hooks
  [gitlab project-id]
  (-> (http/get (format "%s/api/v4/projects/%s/hooks"
                        (:url gitlab)
                        (url-encode project-id))
                {:headers {"PRIVATE-TOKEN" (:token gitlab)}
                 :accept :json})
      :body
      json/parse-string))

(defn add-project-hook
  [gitlab project-id url]
  (let [payload {"url" url
                 "pipeline_events" true
                 "push_events" false}]
    (-> (http/post (format "%s/api/v4/projects/%s/hooks"
                           (:url gitlab)
                           (url-encode project-id))
                   {:headers {"PRIVATE-TOKEN" (:token gitlab)}
                    :body (json/generate-string payload)
                    :accept :json
                    :content-type :json})
        :body
        json/parse-string)))



(defn add-webhooks
  [gitlab projects]
  (if-not (s/valid? ::projects projects)
    (s/explain-str ::projects projects)
    (do
      (info (str "Adding webhooks for: " projects))
      (loop [[project & rst-projects] projects]
        (when-not (nil? project)
          (let [current-hooks
                (->> (get-project-hooks gitlab project)
                     (filter (fn [{url "url"
                                   pipeline-events "pipeline_events"}]
                               (and (= url (:webhook-url gitlab))
                                    pipeline-events))))]
            (when (= 0 (count current-hooks))
              (let [url (:webhook-url gitlab)]
                (info (str "We are adding a hook into gitlab: " url))
                (add-project-hook gitlab project url)))
            (recur rst-projects)))))))

(defn list-triggers
  [gitlab group project]
  (let [url (format "%s/api/v4/projects/%s/triggers"
                    (:url gitlab)
                    (url-encode (str group "/" project)))]
    (let [triggers (http/get url
                             {:headers {"PRIVATE-TOKEN" (:token gitlab)}
                              :accept :json})]
      (-> triggers
          :body
          (json/parse-string true)))))

(defn create-trigger
  [gitlab group project]
  (let [url (format "%s/api/v4/projects/%s/triggers"
                    (:url gitlab)
                    (url-encode (str group "/" project)))]
    (let [r (http/post url
                       {:headers {"PRIVATE-TOKEN" (:token gitlab)}
                        :form-params {"description" "kanaloa"}
                        :accept :json})]
      (-> r
          :body
          (json/parse-string true)
          :token))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;


(s/def ::ref string?)
(s/def ::status #{"pending" "failed" "running" "success" "canceled"})
(s/def ::object_kind #{"pipeline"})
(s/def ::object_attributes
  (s/keys :req-un [::ref
                   ::status]))
(s/def ::path_with_namespace (s/and string? #(re-matches kan-regex/group-project-regex %)))
(s/def ::web_url string?)
(s/def ::project (s/keys :req-un [::path_with_namespace
                                  ::web_url]))
(s/def ::gitlab-pipelines-webhook
  (s/keys :req-un [::object_kind
                   ::object_attributes
                   ::project]))


(defn make-reverse-dependencies-list
  "(Takes an instance of the gitlab system, which has nothing to do with this function! However, we can query for the list of dependencies/declarations).

  Remember that every project that depends on other projects, have to declare this fact to us, with the /self-declare/ HTTP endpoint at runtime. We usually think of the dependencies list as the projects declare them, ie _I_ depend on _those_ projects/refs, etc.

  However, this function can _reverse_ the orientation of that data, by letting things-that-can-change (ie, dependencies) and make them point to the things that depend on them.

  This is useful to figure out which builds to trigger in response to which other builds.

  eg: {:a [1 2 3]
       :b [4 5 6]
       :c [1]}
  -> [[:a 1] [:a 2] [:a 3]
      [:b 4] [:b 5] [:b 6]
      [:c 1]]
  -> [[6 :b] [5 :b] [4 :b]
      [3 :a] [2 :a] [1 :a]
      [1 :c]]
  -> {6 [:b]
      5 [:b]
      4 [:b]
      3 [:a]
      2 [:a]
      1 [:a :c]}
  "
  [{:keys [self-declarations]}]

  (into {}
        (comp

         ;; flatten out, to have the depending thing individually
         ;; with every little thingit depends on
         (mapcat (fn [[depending-prj dependencies]]
                   (map #(vector depending-prj %) dependencies)))

         ;; now reverse the first and the second items, of every pair.
         ;; ie, have the dependency in the first position
         ;; and the depending thing second
         (map (fn [[x y]] [y x]))

         ;; now we need to group by the key/first item
         ;; and thereby gather a list of things that the key
         ;; depends upon.
         (x/by-key first (comp
                          (map second)
                          (x/reduce conj))))
        (decl/get-current-declarations self-declarations)))

(defn get-trigger-token
  "Returns a future, which returns the trigger token"
  [gitlab vault group project]
  (future
    (let [existing-secret (vault/read-secret
                           vault
                           (format "gitlab-triggers/%s/%s"
                                   group
                                   project))]
      (if existing-secret
        ;; the trigger has been created and stored in vault,
        ;; we found it, let's use it, we're done.
        (:token existing-secret)

        ;; ok, the trigger doesn't exist in vault yet.
        ;; we have to query gitlab, to find out if we lost
        ;; a trigger token, if not, create it, store it in vault
        ;; and then return it from the future.
        (let [existing-triggers (list-triggers gitlab group project)
              trigger (->> existing-triggers
                           (filter #(= "kanaloa" (:description %)))
                           (map #(:token %))
                           first)
              ;; There are two possibilities:
              ;; 1) the token exists in 'trigger'. Now, save to vault and return the value
              ;; 2) the token does not exist, and we have to create it, save it to vault, and return it
              trigger (if trigger trigger
                          (create-trigger gitlab group project))]

          (vault/write-secret vault
                              (format "gitlab-triggers/%s/%s"
                                      group project)
                              trigger)
          trigger)))))

(defn webhook-fired
  "Deals with the POSTS made by gitlab to the webhook callbacks"
  [{vault :vault
    consul-backup :consul-backup
    :as gitlab} data]

  ;; if the data is invalid, quit and go'bye
  (if-not (s/valid? ::gitlab-pipelines-webhook data)

    ;; the warning is on our side instead of on the consumer side
    ;; because we're actually gitlab's consumer in this case
    ;; the onus is on us to get it right.
    (warn (str "Cannot accept gitlab webhook POST: "
               (s/explain ::gitlab-pipelines-webhook data)))

    ;; ELSE!
    ;; ie - data is valid, process on a background thread
    ;; and we only want to process anything if the result
    ;; is success (for now)
    (let [{{:keys [ref status]} :object_attributes
           {:keys [path_with_namespace web_url]} :project}
          (s/conform ::gitlab-pipelines-webhook data)

          project-that-posted (format "%s:%s" path_with_namespace ref)]

      (debug (str "Gitlab Webhook: " project-that-posted "; status=" status))

      (consul-backup/swap-db! consul-backup
                              assoc-in
                              [:pipeline-statii project-that-posted]
                              status)

      (when (= "success" status)
        (csp/thread
          (let [deps (-> (make-reverse-dependencies-list gitlab)
                         (get project-that-posted))]
            (info (str "Triggering builds on these projects: " deps))
            (doseq [dep deps]
              (let [[_ group project tag]
                    (re-matches kan-regex/group-project-tag-regex dep)

                    trigger-token (get-trigger-token gitlab vault group project)]

                (http/post (format "%s/api/v4/projects/%s/trigger/pipeline"
                                   (:url gitlab)
                                   (url-encode (str group "/" project)))
                           {:form-params {"token" @trigger-token
                                          "ref" tag
                                          "variables" {"LIMIT_PROJECT_GROUP" path_with_namespace}}
                            :accept :json
                            :content-type :json})))))))))
