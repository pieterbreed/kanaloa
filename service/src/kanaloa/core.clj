(ns kanaloa.core
  (:gen-class)
  (:require [clojure.pprint :as pp]
            [zoona.lib.gelf-timbre-appender.core :as ga]
            [kanaloa.rest :as rest]
            [kanaloa.signals :as signals]
            [kanaloa.self-declare]
            [kanaloa.client-updater]
            [kanaloa.consul-backup]
            [integrant.core :as ig]
            [environ.core :refer [env]]
            [taoensso.timbre :as timbre
             :refer [log  trace  debug  info  warn  error  fatal  report
                     spy get-env]]))


(defn env-or-die
  [key]
  (or (env key)
      (do
        (error (format "MISSING ENVVAR: %s" key))
        (System/exit 1))))

(def default-config
  {:adapter/client-updater {:declarations (ig/ref :adapter/self-declarations)
                            :websocket (ig/ref :adapter/websocket)
                            :consul-backup (ig/ref :adapter/consul-backup)}
   :adapter/self-declarations {:consul-backup (ig/ref :adapter/consul-backup)}
   :adapter/websocket {}
   :adapter/consul-backup {:consul-http-addr (or (env :consul-http-addr)
                                                 "http://consul-ui.zoona-internal")
                           :consul-state-key "kanaloa/db"}
   :adapter/httpkit {:port 8000
                     :declarations (ig/ref :adapter/self-declarations)
                     :websocket (ig/ref :adapter/websocket)
                     :gitlab (ig/ref :adapter/gitlab)}
   :adapter/gitlab {:url "https://gitlab.zoona.io"
                    :self-declarations (ig/ref :adapter/self-declarations)
                    :vault (ig/ref :adapter/vault)
                    :consul-backup (ig/ref :adapter/consul-backup)}
   :adapter/vault {:vault-addr "http://vault.zoona-internal"
                   :vault-secret-path-prepend "secret/systems/kanaloa"}})

(defn make-main-config
  []
  (-> default-config
      (assoc-in [:adapter/gitlab :token] (env-or-die :gitlab-token))
      (assoc-in [:adapter/gitlab :webhook-url] (str (env-or-die :external-app-url)
                                                    "/gitlab-webhook"))
      (assoc-in [:adapter/vault :vault-token] (env-or-die :vault-token))))

(defn -main [& args]
  (try

    ;; GELF LOGS
    ;; we want to patch in the gelf host field if it has been set
    (let [cfg {:application-name (or (some-> :log-facility env)
                                     "kanaloa")}
          cfg (if (not (env :log-host)) cfg
                  (assoc cfg :host (env :log-host)))]
      (timbre/merge-config!
       {:appenders {:println nil
                    :gelf (ga/gelf-appender cfg)}}))

    (let [log-level (some-> (env :log-level "info")
                            hash-set
                            (some #{"debug" "info" "warn"})
                            keyword)]
      (timbre/set-level! log-level)
      (info (str "The LOG LEVEL has been set to " log-level)))


    (let [config (make-main-config)
          system (ig/init config)]

      ;; set uncaught exception handlers, jvm-wide
      (Thread/setDefaultUncaughtExceptionHandler
       (reify Thread$UncaughtExceptionHandler
         (uncaughtException [_ thread ex]
           (fatal ex (format "global uncaught exception handler was called, terminating the app..."))
           (beckon/raise! "INT")
           (Thread/sleep (* 1000 10))
           (beckon/raise! "TERM"))))


      (info "System is running, waiting for Ctrl-C...")
      (signals/wait-for-ctrl-c)
      (warn "Shutting down...")
      (ig/halt! system)
      (info "Goodbye cruel world.")
      (flush))
    (catch Exception e
      (error e)
      (System/exit 1))))
