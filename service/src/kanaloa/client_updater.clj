(ns kanaloa.client-updater
  (:require [kanaloa.consul-backup :as consul-backup]
            [kanaloa.self-declare :as decl]
            [integrant.core :as ig]
            [clojure.core.async :as csp]
            [taoensso.timbre :as timbre
             :refer [log  trace  debug  info  warn  error  fatal  report spy get-env]]))



(defn start-client-updater-thread
  [{:keys [declarations websocket consul-backup]}]
  (let [new-decl-ch (csp/chan)
        new-client-ch (csp/chan)
        new-pipeline-status-ch (csp/chan)
        quit-ch (csp/chan)

        {:keys [new-self-declaration-mult]}
        declarations

        {:keys [new-pipeline-status-mult]}
        consul-backup

        {:keys [new-client-mult
                send-to-all-clients
                send-to-client]}
        websocket]


    (csp/tap new-self-declaration-mult new-decl-ch)
    (csp/tap new-pipeline-status-mult new-pipeline-status-ch)
    (csp/tap new-client-mult new-client-ch)

    (csp/thread (csp/go-loop []
                  (let [[v ch] (csp/alts! [new-decl-ch
                                           new-client-ch
                                           new-pipeline-status-ch
                                           quit-ch])]
                    (debug (str "Client updater received " v))
                    (if (nil? v)
                      (debug "Client updater received nil, quiting...")
                      (do
                        (condp = ch
                          new-pipeline-status-ch (do (send-to-all-clients
                                                      [:kanaloa/pipeline-statii v]))
                          new-decl-ch (do (send-to-all-clients
                                           [:kanaloa/dependencies v]))
                          new-client-ch (do (send-to-client
                                             v
                                             [:kanaloa/dependencies
                                              (decl/get-current-declarations declarations)])
                                            (send-to-client
                                             v
                                             [:kanaloa/pipeline-statii
                                              (consul-backup/get-pipeline-statii consul-backup)])))
                        (recur))))))
    #(do (csp/close! quit-ch))))

(defmethod ig/init-key
  :adapter/client-updater
  [_ x]
  {:stop-fn (start-client-updater-thread x)})

(defmethod ig/halt-key!
  :adapter/client-updater
  [_ {stop-fn :stop-fn}]
  (stop-fn))
