(ns kanaloa.signals
  (:require [beckon]
            [clojure.core.async :as csp]
            [taoensso.timbre :as timbre
             :refer [log  trace  debug  info  warn  error  fatal  report spy get-env]]))

(defn wait-for-ctrl-c
  []
  (let [quit-ch (csp/chan)]
    ;; set uncaught exception handlers, jvm-wide
    (Thread/setDefaultUncaughtExceptionHandler
     (reify Thread$UncaughtExceptionHandler
       (uncaughtException [_ thread ex]
         (fatal ex (format "global uncaught exception handler was called, terminating the app..."))
         (beckon/raise! "INT")
         (Thread/sleep (* 1000 10))
         (beckon/raise! "TERM"))))

    ;; handle Ctrl-C properly
    (let [quit-counter (atom 0)]
      (reset! (beckon/signal-atom "INT")
              #{(fn []
                  (warn (format "Intercepted the INT OS signal (attempt:%s)." @quit-counter))
                  (swap! quit-counter inc)
                  (when (< 1 @quit-counter)
                    (error "Previous invocations of the stop-handler did not succeed. Force killing.")
                    (beckon/raise! "TERM"))
                  (csp/>!! quit-ch true)
                  (csp/close! quit-ch))}))

    ;; wait until Ctrl-C is intercepted
    (csp/<!! quit-ch)
    (info "The application is now shutting down...")))
