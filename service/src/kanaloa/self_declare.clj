(ns kanaloa.self-declare
  (:require [kanaloa.consul-backup :as consul-backup]
            [clojure.core.async :as csp]
            [clojure.spec.alpha :as s]
            [clojure.string :as str]
            [integrant.core :as ig]
            [taoensso.timbre :as timbre
             :refer [log  trace  debug  info  warn  error  fatal  report spy get-env]]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod ig/init-key
  :adapter/self-declarations
  [_ {:keys [consul-backup]
      :as system}]
  (let [app-db (-> consul-backup :current-value)
        declarations (atom nil)
        new-value-ch (csp/chan)]

    ;; We're going to be doing trickle-down economics here.
    ;; We're selecting from the app db into the declarations atom,
    ;; then from there, listening for changes only, we
    ;; propagate values to the clients.
    (add-watch declarations
               :declarations-changed
               (fn [_ _ old-value new-value]
                 (when (not= old-value new-value)
                   (csp/go (csp/>! new-value-ch new-value)))))
    (add-watch app-db
               :declarations-changed
               (fn [_ _ old-value new-value]
                 (when (not= old-value new-value)
                   (reset! declarations (:self-declarations new-value)))))

    (merge system
           {:new-self-declaration-mult (csp/mult new-value-ch)
            :declarations declarations
            :cleanup-fn #(do (remove-watch app-db :declarations-changed)
                             (remove-watch declarations :declarations-changed)
                             (csp/close! new-value-ch))})))

(defmethod ig/halt-key!
  :adapter/self-declarations
  [_ {:keys [cleanup-fn]}]
  (cleanup-fn))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn get-current-declarations
  [{declarations :declarations}]
  @declarations)

(defn new-declaration!
  [{:keys [consul-backup]
    :as self-declare-system} depending-project dependencies]
  (if-not (and (s/valid? :kanaloa.consul-backup/group-project-tag depending-project)
               (s/valid? :kanaloa.consul-backup/dependencies dependencies))
    (let [spec-error (format "New declaration failed with bad data.\nproject: %s\ndeps: %s"
                             (s/explain-str :kanaloa.consul-backup/group-project-tag depending-project)
                             (s/explain-str :kanaloa.consul-backup/dependencies dependencies))]
      (warn spec-error)
      spec-error)
    (do (consul-backup/swap-db! consul-backup
                                assoc-in
                                [:self-declarations depending-project]
                                dependencies)
        nil)))
