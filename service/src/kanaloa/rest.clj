(ns kanaloa.rest
  (:use org.httpkit.server)
  (:require [kanaloa.common.regex :as kan-regex]
            [kanaloa.gitlab :as gitlab]
            [kanaloa.websocket :as websocket]
            [kanaloa.self-declare :as self-declare]
            [kanaloa.consul-backup :as consul-backup]
            [integrant.core :as ig]
            [ring.util.response :as resp-utils]
            [ring.util.request :as req-utils]
            [ring.middleware.defaults :as middleware]
            [ring.middleware.anti-forgery :as rmw-af]
            [ring.middleware.session :as rmw-session]
            [taoensso.timbre :as timbre
             :refer [log  trace  debug  info  warn  error  fatal  report spy get-env]]
            [compojure.core :refer :all]
            [compojure.route :as route]
            [clojure.java.io :as io]
            [ring.middleware.json]
            [ring.middleware.gzip :as resp-gz]))


(defonce counter (atom 0))
(comment

  (debug (format "And the counter value is: %s" @counter))

  (println "this is some magic"))



(comment)


(defn makeroutes
  [declarations-system
   {:keys [ajax-get-or-ws-handshake-fn
           ajax-post-fn]
    :as websockets}
   gitlab]
  (routes
   ;; websocket code
   (GET "/chsk" req (ajax-get-or-ws-handshake-fn req))

   (POST "/chsk" req (ajax-post-fn req))

   ;; static content, we bake this into the classpath
   ;; it should really be served from a CDN
   (GET [":index" :index #"/|/index.html"]
        [path :as req]
        (-> "/index.html"
            resp-utils/resource-response
            (resp-utils/content-type "text/html")))
   (GET "/banner.txt"
        _
        (let [banners ["/banner.txt"
                       "/octopi/jgs.txt"
                       "/octopi/medium.txt"
                       "/octopi/moop.txt"
                       "/octopi/oh-hai.txt"
                       "/octopi/shimrod.txt"]
              todays-i (-> banners count rand-int)
              banner-resource (get banners todays-i)]
          (-> banner-resource
              resp-utils/resource-response
              (resp-utils/content-type "text/plain"))))
   (GET [":csspath" :csspath #"/css/main.css"]
        [csspath :as req]
        (-> csspath
            resp-utils/resource-response
            (resp-utils/content-type "text/css")))
   (GET [":jspath" :jspath #"/js/.*"]
        [jspath :as req]
        (-> jspath
            resp-utils/resource-response
            (resp-utils/content-type "text/javascript")))
   (GET "/config.edn"
        [_]
        (-> (pr-str {:kanaloa.core/log-level :debug
                     :kanaloa.core/wss-uri "http://localhost:8000/chsk"})
            resp-utils/response
            (resp-utils/content-type "application/edn")))

   (POST "/gitlab-webhook"
         {:keys [body headers] :as req}
         (gitlab/webhook-fired gitlab body)
         (resp-utils/response (str "OK")))


   ;; validates the request data, if it returns non-nil
   ;; return that as an error resonse
   ;; otherwise, just continue
   (POST ["/self-declare/:project" :project kan-regex/group-project-tag-regex]
         [project :as {dep-data :body}]
         (let [dep-data (set dep-data)]
           (info (format "%s just self-declared its dependencies as %s"
                         project
                         dep-data))
           (let [result (self-declare/new-declaration! declarations-system
                                                       project
                                                       dep-data)]
             (if result
               (resp-utils/bad-request result)
               (do
                 (-> (gitlab/add-webhooks
                      gitlab
                      (into []
                            (comp
                             (map (partial re-matches kan-regex/group-project-tag-regex))
                             (map (fn [[_ group project _]] (format "%s/%s" group project))))
                            (conj dep-data project)))
                     resp-utils/response))))))

   (GET "/healthcheck" _
        (-> "OK"
            resp-utils/response
            (resp-utils/content-type "text/plain")))
   (route/not-found
    "<html><body><h1>Oh noes! I don't have what you are looking for :(</h1></body></html>")))


(defn wrap-exception-handling
  [handler]
  (fn [request]
    (try
      (handler request)
      (catch Exception e
        (error e)
        {:status 400
         :body "Invalid request; check the logs for details..."}))))

(defmethod ig/init-key :adapter/httpkit
  [_ {:keys [declarations websocket gitlab] :as opts}]
  (run-server (-> (makeroutes declarations websocket gitlab)
                  (middleware/wrap-defaults middleware/api-defaults)
                  (wrap-exception-handling)
                  (ring.middleware.json/wrap-json-body {:keywords? true})
                  (resp-gz/wrap-gzip)
                  (rmw-session/wrap-session))
              (-> {:port 8080}
                  (merge opts)
                  (dissoc :handler))))

(defmethod ig/halt-key! :adapter/httpkit [_ stop-server]
  (stop-server))
