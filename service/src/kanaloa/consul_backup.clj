(ns kanaloa.consul-backup
  (:require [kanaloa.common.regex :as kan-regex]
            [clojure.string :as str]
            [clojure.spec.alpha :as s]
            [clojure.tools.reader.edn :as edn]
            [integrant.core :as ig]
            [clj-http.client :as http]
            [clojure.core.async :as csp]
            [taoensso.timbre :as timbre
             :refer [log  trace  debug  info  warn  error  fatal  report spy get-env]]))


(defn -get-db-value
  "Gets the current/latest db value from consul."
  ([consul-http-addr consul-state-key]
   (-get-db-value consul-http-addr
                  consul-state-key
                  nil nil))
  ([consul-http-addr consul-state-key index timeout-s]
   (let [req {:throw-exceptions false
              :query-params {:raw true}}
         req (if (and index timeout-s)
               (-> req
                   (assoc-in [:query-params :index] index)
                   (assoc-in [:query-params :wait] (str timeout-s "s")))
               req)
         get-fn #(http/get (format "%s/v1/kv/%s"
                                   consul-http-addr
                                   consul-state-key)
                           req)
         r  (loop [r (get-fn)]
              (if (= (:status r) 200) r
                  (do (Thread/sleep 250)
                      (recur (get-fn)))))]

     (with-meta
       (-> r :body edn/read-string (or {}))
       {:x-consul-index (-> r
                            :headers
                            (get "X-Consul-Index")
                            (or "0")
                            (#(Integer/parseInt %)))}))))


(defn -set-db-value
  "Accepts a modified value and persists to consul. Only allow to set the db with a pos-int? index"
  [consul-http-addr consul-state-key index v]
  (when-not (nat-int? index)
    (throw (Exception. "index needs to be set properly, non-zero, pos-int? style.")))
  (let [req {:throw-exceptions false
             :query-params {:cas index}
             :body (pr-str v)}
        resp (http/put (format "%s/v1/kv/%s"
                               consul-http-addr
                               consul-state-key)
                       req)]
    resp))

(comment

  (-get-db-value "http://consul-ui.zoona-internal"
                 "kanaloa/db"))




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;


(s/def ::group-project-tag (s/and string? #(re-matches kan-regex/group-project-tag-regex %)))
(s/def ::dependencies (s/coll-of ::group-project-tag
                                 :kind set?))
(s/def ::self-declarations (s/map-of ::group-project-tag
                                     ::dependencies))
(s/def ::pipeline-statii (s/map-of ::group-project-tag
                                   #{"pending"
                                     "failed"
                                     "running"
                                     "success"
                                     "canceled"}))
(s/def ::db (s/keys :opt-un [::self-declarations
                             ::pipeline-statii]))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;

(defn swap-db!
  [{:keys [current-value
           consul-http-addr
           consul-state-key]
    :as consul} swap-fn & rst]
  (let [current-value (-> consul :current-value deref)
        current-index (-> current-value
                          meta
                          :x-consul-index)
        new-value (apply swap-fn current-value rst)]
    (if-not (s/valid? ::db new-value)
      (throw (ex-info "The new value provided by the swap-fn does not conform to the ::db spec."
                      (s/explain-data ::db new-value)))
      (-set-db-value consul-http-addr
                     consul-state-key
                     current-index
                     (apply swap-fn current-value rst)))))

(defn current-value
  [gitlab]
  (-> gitlab :current-value deref))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;

(defn watcher-thread-main
  [{:keys [consul-http-addr consul-state-key]
    :as gitlab}
   current-value
   quit-ch]
  (let [first-value (-get-db-value consul-http-addr
                                   consul-state-key)]
    (reset! current-value first-value)

    (loop [index (-> @current-value meta :x-consul-index)]
      (let [[v ch] (csp/alts!! [quit-ch (csp/thread
                                          (-get-db-value consul-http-addr
                                                         consul-state-key
                                                         index
                                                         10))])]
        (if (= ch quit-ch) true
            (do
              (let [new-index (-> v meta :x-consul-index)]
                (when (not= new-index index)
                  (let [nv (reset! current-value v)]
                    (debug (str "reset!-ing the consul atom to: " nv))))
                (recur new-index))))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;

(defmethod ig/init-key
  :adapter/consul-backup
  [_ {:keys [consul-http-addr consul-state-key]
      :as gitlab}]
  ;; we want to start polling for the value in consul
  ;; when a new value returns from consul, we want to replace
  ;; our own current value with the new value.
  ;; when we update our own value, we want to make the same change
  ;; to the consul value, so it can propagate to other clients
  ;; and ourselves when we restart.
  (let [current-value (atom nil)
        new-pipeline-status-ch (csp/chan)
        q (csp/chan)]

    (add-watch current-value
               :pipeline-statii-updated
               (fn [_ _ old-value new-value]
                 (when (not= (:pipeline-statii old-value)
                             (:pipeline-statii new-value))
                   (csp/go (csp/>! new-pipeline-status-ch
                                   (:pipeline-statii new-value))))))

    ;; this thread will keep the value in current-value up-to-date with consul's
    (csp/thread
      (watcher-thread-main gitlab current-value q))
    (merge gitlab
           {:stop #(do (csp/close! q)
                       (remove-watch current-value :pipeline-statii-updated))
            :current-value current-value
            :quit-ch q
            :new-pipeline-status-mult (csp/mult new-pipeline-status-ch)})))


(defmethod ig/halt-key!
  :adapter/consul-backup
  [_ {:keys [stop]}]
  (stop))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;

(defn get-pipeline-statii
  [{current-value :current-value}]
  (-> current-value deref :pipeline-statii))
