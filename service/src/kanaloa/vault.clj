(ns kanaloa.vault
  (:require [clj-http.client :as http]
            [clojure.core.async :as csp]
            [clojure.spec.alpha :as s]
            [ring.util.codec :refer [url-encode]]
            [integrant.core :as ig]
            [cheshire.core :as json]
            [net.cgrand.xforms :as x]
            [taoensso.timbre :as timbre
             :refer [log  trace  debug  info  warn  error  fatal  report spy get-env]]))

(defmethod ig/init-key :adapter/vault
  [_ opts]
  opts)

(defn read-secret
  [{:keys [vault-addr
           vault-token
           vault-secret-path-prepend]} secret-key]
  (let [resp  (http/get (format "%s/v1/%s/%s"
                                vault-addr
                                vault-secret-path-prepend
                                secret-key)
                        {:headers {"X-Vault-Token" vault-token}
                         :throw-exceptions false})
        body (:body resp)]
    (when body (-> body (json/parse-string true) (get :data)))))

(defn write-secret
  [{:keys [vault-addr
           vault-token
           vault-secret-path-prepend]} secret-key token]
  (-> (http/post (format "%s/v1/%s/%s"
                         vault-addr
                         vault-secret-path-prepend
                        secret-key)
                 {:headers {"X-Vault-Token" vault-token}
                  :body (json/generate-string {"token" token})})
      :body))
