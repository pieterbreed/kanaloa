(ns user
  (:require [integrant.core :as ig]
            [kanaloa.rest :as r3st]
            [kanaloa.gitlab :as gitlab]
            [kanaloa.core :as core]
            [kanaloa.consul-backup :as consul-backup]
            [kanaloa.vault :as vault]
            [clojure.string :as str]
            [clj-http.client :as http]
            [taoensso.timbre :as timbre
             :refer [log  trace  debug  info  warn  error  fatal  report
                     logf tracef debugf infof warnf errorf fatalf reportf
                     spy get-env]]
            [cheshire.core :as json]
            [clojure.java.io :refer [reader]]))

(println "User namespace being loaded")

(defonce gitlab-token (atom nil))
(defonce vault-token (atom nil))

(comment


  (defn set-vault-token [s] (reset! vault-token s))

  ;; to use this, I created a personal gitlab token here
  ;; https://gitlab.zoona.io/profile/personal_access_tokens
  ;;
  ;; then I pasted that into ~/.gitlab_token
  ;; and it can be read from there below.
  (->> (reset! gitlab-token (-> (str (System/getProperty "user.home") "/" ".gitlab_token")
                               reader
                               slurp
                               str/trim))
       ;; this hides the actual value of the secret,
       ;; even from the user display, sorry...
       (map (constantly \*))
       (apply str))

  )

(def full-app-uri "http://localhost:8000")
(defn make-dev-cfg
  []
  (-> core/default-config
      (update-in [:adapter/gitlab]
                 (fn [x]
                   (assoc x
                          :token @gitlab-token
                          :webhook-url "https://kanaloa.centaur.zoona.network/gitlab-webhook")))
      (assoc-in [:adapter/vault :vault-token] @vault-token)))

(defonce system (atom (make-dev-cfg)))

(defn go
  []
  (reset! system (ig/init (make-dev-cfg))))

(defn stop
  []
  (swap! system ig/halt!))

(defn reset
  []
  (stop)
  (go))


(defn gitlab-system [] (-> system deref :adapter/gitlab ))
(defn vault-system [] (-> system deref :adapter/vault))

(comment

  (vault/read-secret (vault-system) "secret/systems/kanaloa/gitlab")
  
  )


(comment
  (debug "testing")

  (go)
  (stop)
  (reset)


  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  (let [declaring-project-id "fake-group/fake-project:faketag"]
    (http/post (str full-app-uri
                    "/self-declare/"
                    declaring-project-id)
               {:body "{\"docker-registry.zoona.io/tachyon/monolith-comms/launcher\":{\"ref\":\"master\",\"project\":\"tachyon/monolith-comms\"},\"docker-registry.zoona.io/ops-engineering/kanaloa/launcher\":{\"ref\":\"master\",\"project\":\"ops-engineering/kanaloa\"}}"
                :content-type :json}))

  

  
  (gitlab/add-hooks-for-dependencies (gitlab-system)
                                     {"tachyon/monolith-comms/launcher"
                                      {:ref "master"
                                       :project "ops-engineering/kanaloa"}})

  (gitlab/get-project-hooks (gitlab-system)
                            "tachyon/monolith-comms")

  (gitlab/add-project-hook (:adapter/gitlab integrant.repl.state/system)
                           "tachyon/monolith-comms"
                           "https://kanaloa.centaur.zoona.network/project-hook")

  

  )



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;

(comment

  (def consul-cfg {:adapter/consul-backup {:consul-http-addr "http://consul-ui.zoona-internal"
                                           :consul-state-key "kanaloa/db"}})
  (defonce consul-system (atom nil))
  
  (defn init-consul-system
    []
    (reset! consul-system 
            (ig/init consul-cfg)))
  (defn stop-consul-system
    []
    (ig/halt! @consul-system)
    (reset! consul-system nil))

  (init-consul-system)
  (stop-consul-system)

  
  (consul-backup/current-value (-> consul-system deref :adapter/consul-backup))
  (consul-backup/swap-db! (-> consul-system deref :adapter/consul-backup)
                          assoc-in
                          [:self-declarations "group/project:tag"] #{"group/project:tag"})
  


  )
