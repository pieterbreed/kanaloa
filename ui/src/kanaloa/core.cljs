(ns kanaloa.core
  (:require-macros [cljs.core.async.macros :refer [go go-loop]])
  (:require [kanaloa.common.regex :as kan-regex]
            [kanaloa.banner :refer [default-banner]]
            [taoensso.timbre
             :as timbre
             :refer-macros [log  trace  debug  info  warn  error  fatal  report
                            logf tracef debugf infof warnf errorf fatalf reportf
                            spy get-env]]
            [cljs.spec.alpha :as s]
            [reagent.core :as reagent]
            [re-frame.core :as rf]
            [cljs-http.client :as http]
            [clojure.string :as str]
            [cljs.core.async :refer [chan <! >! timeout close!]]
            [taoensso.sente  :as sente]
            [day8.re-frame.http-fx]
            [ajax.core :as ajax :refer [text-response-format raw-response-format]]
            [ajax.edn :refer [edn-response-format]]
            [cemerick.url :refer [url]]
            [goog.string :as gstring]
            [goog.string.format]))


(enable-console-print!)
(info "Hello world. Pieter was here.")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;

(s/def ::log-level #{:debug :info})
(s/def ::config-status #{:loading :failed :success})
(s/def ::wss-status #{:unknown :open :not-open})
(s/def ::wss-ur string?)
(s/def ::app-config (s/keys :req [::log-level ::wss-uri]
                            :opt [::config-status]))


(s/def ::group-project-tag (s/and string? #(re-matches kan-regex/group-project-tag-regex %)))

(s/def ::dependencies (s/map-of ::group-project-tag
                                (s/coll-of ::group-project-tag
                                           :kind set?)))

(s/def ::pipeline-status #{"pending"
                           "failed"
                           "running"
                           "success"
                           "canceled"})
(s/def ::pipeline-statii (s/map-of ::group-project-tag
                                   ::pipeline-status))


(s/def ::banner (s/coll-of string?))

(s/def ::config-is-loading boolean?)
(s/def ::app-db (s/keys :req [::config-is-loading
                              ::wss-status]
                        :opt [::app-config
                              ::dependencies
                              ::banner
                              ::pipeline-statii]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;


(declare chsk)
(declare ch-chsk)
(declare chsk-send!)
(declare chsk-state)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;

(defmulti -server-push-event
  "Multimethod to deal with data being pushed to the client from the server."
  first)

(defmethod -server-push-event
  :default
  [& s]
  (debugf "Received unknown things from the server: %s" (str s)))


(defmethod -server-push-event
  :chsk/ws-ping
  [& _]
  (debug "Server says PING!"))

(defmethod -server-push-event
  :kanaloa/dependencies
  [[_ deps]]
  (debugf "Got some dependencies from the server; %s" deps)
  (rf/dispatch [:dependencies-updated deps]))

(defmethod -server-push-event
  :kanaloa/pipeline-statii
  [[_ statii]]
  (debugf "Got some pipeline-statii from the server; %s" statii)
  (rf/dispatch [:pipeline-statii-updated statii]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;

(defmulti -event-msg-handler
  "Multimethod to handle Sente `event-msg`s"
  :id)


(defn event-msg-handler
  "Wraps `-event-msg-handler` with logging, error catching, etc."
  [{:as ev-msg :keys [id ?data event]}]
  (-event-msg-handler ev-msg))

(defmethod -event-msg-handler
  :default ; Default/fallback case (no other matching handler)
  [{:as ev-msg :keys [event]}]
  (infof "Unhandled event: %s" event))

(defn chsk-state->wss-status
  [state-map]
  (if (-> state-map :open?)
      :open
      :not-open))

(defmethod -event-msg-handler :chsk/state
  [{:as ev-msg :keys [?data]}]
  (let [[old-state-map new-state-map] ?data]
    (rf/dispatch [:wss-status-changed (chsk-state->wss-status new-state-map)])
    (if (:first-open? new-state-map)
      (infof "Channel socket successfully established!: %s" new-state-map)
      (infof "Channel socket state change: %s"              new-state-map))))

(defmethod -event-msg-handler :chsk/recv
  [{:as ev-msg :keys [?data]}]
  (-server-push-event ?data))

(defmethod -event-msg-handler :chsk/handshake
  [{:as ev-msg :keys [?data]}]
  (let [[?uid ?csrf-token ?handshake-data] ?data]
    (infof "Handshake: %s" ?data)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;



(defn setup-websocket!
  [wss-uri]
  (let [wss-uri (url wss-uri)]
    (infof "Setting up websocket to connect to %s" wss-uri)
    (let [{:keys [chsk ch-recv send-fn state]}
          (sente/make-channel-socket! (:path wss-uri)
                                      {:type :auto
                                       :host (str (:host wss-uri) ":" (:port wss-uri))
                                       :protocol (:protocol wss-uri)})]
      (def chsk       chsk)
      (def ch-chsk    ch-recv)
      (def chsk-send! send-fn)
      (def chsk-state state)

      (sente/start-client-chsk-router! ch-chsk event-msg-handler))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;

(defn check-and-throw
  [a-spec db]
  (when-not (s/valid? a-spec db)
    (do
      (let [msg (str "spec check failed: " (s/explain-str a-spec db))]
        (error msg)
        (throw (ex-info msg {}))))))

(def check-spec-interceptor (rf/after (partial check-and-throw ::app-db)))

(rf/reg-fx
 :set-app-config
 (fn [{lvl ::log-level
       wss ::wss-uri
       :as cfg}]
   (info "Setting up app config")
   (when lvl
     (infof "Setting logging level to $0." lvl)
     (timbre/set-level! lvl)
     (debug "Here is a DEBUG message."))
   (infof "Here is the wss %s" wss)
   (setup-websocket! wss)))


(rf/reg-event-db
 :error-fetching-config-from-server
 [check-spec-interceptor]
 (fn [db v]
   (error (str "db=" db "; Receiving an error response from trying to fetch config." v))
   (assoc db ::config-status :failed)))

(rf/reg-event-db
 :error-fetching-banner
 [check-spec-interceptor]
 (fn [db v]
   (warn "Boo. No banner on the srever.")
   (assoc db ::banner default-banner)))

(rf/reg-event-db
 :received-banner
 [check-spec-interceptor]
 (fn [db [_ v]]
   (let [lines (str/split v "\n")]
     (debugf "Received a banner; %s" (->> lines (take 2) (reduce str)))
     (assoc db ::banner lines))))

(rf/reg-event-db
  :wss-status-changed
  [check-spec-interceptor]
  (fn [db [_ status]]
    (assoc db ::wss-status status)))

(rf/reg-event-db
  :dependencies-updated
  [check-spec-interceptor]
  (fn [db [_ deps]]
    (assoc db ::dependencies deps)))

(rf/reg-event-db
  :pipeline-statii-updated
  [check-spec-interceptor]
  (fn [db [_ statii]]
    (assoc db ::pipeline-statii statii)))

(rf/reg-event-fx
 :received-config-from-server
 [check-spec-interceptor]
 (fn [{:keys [db]} [_ v]]
   (debug (str "Received config file from the server:" db " -> " v))
   {:db (assoc db ::app-config v
                  ::config-status :success)
    :set-app-config v
    :http-xhrio {:method :get
                 :uri "banner.txt"
                 :response-format (raw-response-format)
                 :on-success [:received-banner]
                 :on-failure [:error-fetching-banner]}}))

(rf/reg-event-fx
 :initialize
 [check-spec-interceptor]
 (fn [_ _]
   (debug "Initializing the app...")
   {:db {::config-is-loading true
         ::wss-status :unknown}
    :http-xhrio {:method :get
                 :uri "config.edn"
                 :response-format (edn-response-format)
                 :on-success [:received-config-from-server]
                 :on-failure [:error-fetching-config-from-server]}}))


(rf/reg-sub
 :config
 (fn [{cfg ::app-config
       cfg-status ::config-status}]
   [cfg-status cfg]))

(rf/reg-sub
  :wss-status
  (fn [{wss-status ::wss-status}]
    wss-status))

(rf/reg-sub
  :dependencies
  (fn [{deps ::dependencies}]
    deps))

(rf/reg-sub
  :pipeline-statii
  (fn [{statii ::pipeline-statii}]
    statii))

(rf/reg-sub
  :banner
  (fn [{banner ::banner}]
    banner))

(comment)




(defn make-alert
  [{:keys [color heading detail]}]
  [:div
         [:div {:role "alert"
                :class (str/join " " [(gstring/format "bg-%s-lightest" color)
                                      (gstring/format "border-%s-light" color)
                                      (gstring/format "text-%s-dark" color)
                                      "text-base"
                                      "border px-4 py-3 mb-2 rounded relative"])}
          [:strong {:class "font-bold"} heading]
          (when detail [:span {:class "block float-none"} detail])]])

(defn show-config-warning
  []
  (let [[cfg-status cfg] @(rf/subscribe [:config])]
    (let [is-valid (s/valid? ::app-config cfg)
          not-valid-exp (s/explain-str ::app-config cfg)]
      (cond
        is-valid nil

        (= :loading cfg-status) (make-alert {:color "orange"
                                             :heading "Loading config..."})
        (= :failed cfg-status) (make-alert {:color "red"
                                            :heading "Unable to load the config."})

        :default (make-alert {:color "red"
                              :heading "Can't establish contact with config."
                              :detail not-valid-exp})))))

(defn show-wss-warning
  []
  (let [wss-status @(rf/subscribe [:wss-status])]
    (condp = wss-status
      :open nil #_(make-alert {:color "green"
                               :heading "Making web-sockets Great Again! MwsGA!"})
      :not-open (make-alert {:color "red"
                             :heading "The websocket is not connected at the moment."})
      (make-alert {:color "orange"
                   :heading "Hold on to your hats, this storm should blow over soon..."}))))


(defn show-raw-dependencies
  []
  (let [deps @(rf/subscribe [:dependencies])]
    [:code (str deps)]))

(defn dependencies
  []
  (let [deps @(rf/subscribe [:dependencies])
        statii @(rf/subscribe [:pipeline-statii])
        project->color (fn [p] (condp = (get statii p)
                                 "pending" "orange"
                                 "failed" "red"
                                 "running" "blue"
                                 "success" "green"
                                 "grey"))]

    (conj
     [:div [:h2 {:class "inline-block m-b-2 text-l font-semibold border border-green bg-green-lightest p-1 mb-4 text-green-dark"}
                "Projects waiting to be notified"]]
     (into [:div {:class "flex flex-wrap"}]
           (map (fn [[top-project top-project-dependencies]]
                    [:div {:class (str/join " " ["flex-1 p-2 mr-5 mb-5 max-w-sm border "
                                                 (str "border-" (project->color top-project) "-light")
                                                 (str "bg-" (project->color top-project) "-lightest")
                                                 "shadow-md"])}
                          [:span {:class "text-l font-semibold"}
                                 (str top-project)]
                          [:ul {:class "pl-3"}
                               (map (fn [dep]
                                        ^{:key (str top-project "->" dep)}
                                        [:li [:code {:class (str "text-xs whitespace-no-wrap text-"
                                                                 (project->color dep)
                                                                 "-dark")}
                                                    dep]])
                                    top-project-dependencies)]])
                deps)))))

(defn show-banner
  []
  (let [banner @(rf/subscribe [:banner])
        _ (spy banner)
        [width height] [(->> banner
                             (map count)
                             (apply max)
                             (* 8))
                        (->> banner
                             count
                             (* 15))]]
    (debugf "[width height] = [%s %s]" width height)
    [:div {:class "max-w-lg"}
          [:svg {:viewBox (str "0 0 " width " " height)
                 :xmlns "http://www.w3.org/2000/svg"
                 :class "h-48"}
                [:style (str/join "\n" [".fw { font-family: 'Robo Mono', monospace; "
                                        "      font-size: 13px;"
                                        "      font-weight: 900;"
                                        "      white-space: pre; "
                                        "}"])]
                (->> banner
                     (map #(hash-map :x 0
                                     :y (* %1 15)
                                     :txt %2)
                          (range))
                     (map (fn [{:keys [x y txt]}]
                              [:text {:x x :y y :class "fw"
                                      "xml:space" "preserve"}
                                     [:tspan {:class "fw"
                                              "xml:space" "preserve"}
                                             txt]])))]]))

(defn show-legend
  []
  [:div {:class "text-base text-sm"}
        [:h3 "Colour legend"]
        [:ul {:class "list-reset"}
             [:li [:span {:class "bg-red text-red"} "@"] " failed"]
             [:li [:span {:class "bg-green text-green"} "@"] " success"]
             [:li [:span {:class "bg-orange text-orange"} "@"] " pending"]
             [:li [:span {:class "bg-blue text-blue"} "@"] " running"]
             [:li [:span {:class "bg-grey text-grey"} "@"] " unknown"]]])

(defn ui
  []
  [:div.pt-2.pl-2.pr-2
   [show-config-warning]
   [show-wss-warning]
   [show-banner]
   #_[show-raw-dependencies]
   [dependencies]
   [show-legend]])


(defn ^:export run
  []
  (info "Running...")
  (rf/dispatch-sync [:initialize])
  (reagent/render [ui] (.getElementById js/document "app")))

(defn reload! []
  (info "reload! was called.")
  (run))

(defn main! []
  (info "main! was called.")
  (run))
