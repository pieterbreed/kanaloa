(ns kanaloa.common.regex)

(def group-project-regex #"([-\w\.]+)/([-\w\.]+)")
(def group-project-tag-regex #"([-\w\.]+)/([-\w\.]+):([-\w\.]+)")

(comment

  (re-matches group-project-regex "richard.levey/bulk-distbursement-react")
  (re-matches group-project-tag-regex "ops-engineering/kanaloa:1.0.0")


  )
